// 24.- Comprobar si un número mayor o igual que la unidad es primo.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const esPrimo = (num) => {
  if (num <= 1) return false;
  if (num <= 3) return true;
  if (num % 2 === 0 || num % 3 === 0) return false;
  let i = 5;
  while (i * i <= num) {
    if (num % i === 0 || num % (i + 2) === 0) return false;
    i += 6;
  }
  return true;
};

rl.question("Introduce un número mayor o igual a 1: ", (numero) => {
  const num = parseInt(numero);
  if (!isNaN(num) && num >= 1) {
    if (esPrimo(num)) {
      console.log(`${num} es primo.`);
    } else {
      console.log(`${num} no es primo.`);
    }
  } else {
    console.log("Por favor, introduce un número válido mayor o igual a 1.");
  }
  rl.close();
});
//Emiliano Loya Flores
