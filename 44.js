// 44.- Con el archivo DATOS.DAT listar todos los registros cuyo estado sea uno determinado que
// introduciremos por teclado.

// Suponiendo que estamos trabajando en un entorno de Node.js y tenemos acceso a módulos para manipular archivos

const fs = require("fs");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const listarRegistrosPorEstado = (estado) => {
  fs.readFile("DATOS.DAT", "utf8", (err, data) => {
    if (err) throw err;
    const registros = data.split("\n");
    const registrosFiltrados = registros.filter((reg) => {
      const campos = reg.split(",");
      return campos[4] === estado;
    });
    console.log(`Registros con estado "${estado}":`);
    registrosFiltrados.forEach((reg) => console.log(reg));
    rl.close();
  });
};

rl.question("Introduce el estado a buscar: ", (estado) => {
  listarRegistrosPorEstado(estado);
});
//Emiliano Loya Flores
