// 40.- Una empresa guarda las ventas realizadas por sus tres representantes a lo largo de doce
// meses de sus cuatro productos, VENTAS( representante, mes, producto ). Queremos proyectar el
// total de ventas, TOTAL ( mes, producto ), para lo cual sumamos las ventas de cada producto de
// cada mes de todos los representantes. Imprimir toda la información.

// Suponiendo que tenemos un array llamado VENTAS que contiene las ventas realizadas por cada representante en cada mes para cada producto

// Función para calcular el total de ventas por mes y producto
const calcularTotalVentas = (ventas) => {
  const total = [];
  for (let mes = 0; mes < 11; mes++) {
    total[mes] = [];
    for (let producto = 0; producto < 3; producto++) {
      let sumaVentas = 0;
      for (let representante = 0; representante < 2; representante++) {
        sumaVentas += ventas[representante][mes][producto];
      }
      total[mes][producto] = sumaVentas;
    }
  }
  return total;
};

const VENTAS = [
  // Representante 1
  [
    [100, 200, 150, 300],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [130, 240, 170, 320],
  ],
  // Representante 2
  [
    [100, 200, 150, 300],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [130, 240, 170, 320],
  ],
  // Representante
  [
    [100, 200, 150, 300],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [120, 220, 160, 310],
    [130, 240, 170, 320],
  ],
];
// Supongamos que hemos llenado VENTAS con los datos correspondientes

const TOTAL = calcularTotalVentas(VENTAS);
console.log("Total de ventas por mes y producto:");
console.table(TOTAL);
//Emiliano Loya Flores
