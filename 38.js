// 38.-Ordenar una matriz de M filas y N columnas por la primera columna utilizando el método
// SHELL (por inserción).

// Función para ordenar una matriz por la primera columna usando el método Shell
const ordenarMatrizPorPrimeraColumna = (matriz) => {
  const m = matriz.length;
  const n = matriz[0].length;
  // Método de ordenamiento Shell
  for (let gap = Math.floor(m / 2); gap > 0; gap = Math.floor(gap / 2)) {
    for (let i = gap; i < m; i++) {
      const temp = matriz[i];
      let j;
      for (j = i; j >= gap && matriz[j - gap][0] > temp[0]; j -= gap) {
        matriz[j] = matriz[j - gap];
      }
      matriz[j] = temp;
    }
  }
  return matriz;
};

// Ejemplo de uso
const matrizEjemplo = [
  [3, 5, 7],
  [1, 2, 3],
  [4, 6, 8],
  [2, 3, 4],
];

console.log("Matriz original:");
console.table(matrizEjemplo);

console.log("Matriz ordenada por la primera columna:");
console.table(ordenarMatrizPorPrimeraColumna(matrizEjemplo));
//Emiliano Loya Flores
