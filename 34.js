// 34.- Usando el segundo ejemplo, hacer un programa que busque una nota en la lista.

const buscarNota = (calificaciones, nota) => {
  return calificaciones.includes(nota);
};

const calificaciones = [8, 7, 6, 9, 8, 10, 7, 8, 9, 5];

const notaABuscar = 7;

if (buscarNota(calificaciones, notaABuscar)) {
  console.log(`La nota ${notaABuscar} se encuentra en la lista.`);
} else {
  console.log(`La nota ${notaABuscar} no se encuentra en la lista.`);
}
//Emiliano Loya Flores
