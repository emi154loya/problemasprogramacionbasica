// 37.-Cargar las notas de los alumnos de un colegio en función del número de cursos y del número
// de alumnos por curso.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let cursos = [];
let totalNotas = 0;
let totalAlumnos = 0;

rl.question("Introduce el número de cursos: ", (numCursos) => {
  const num = parseInt(numCursos);
  if (!isNaN(num) && num > 0) {
    for (let i = 1; i <= num; i++) {
      rl.question(
        `Introduce el número de alumnos para el curso ${i}: `,
        (numAlumnos) => {
          const numA = parseInt(numAlumnos);
          if (!isNaN(numA) && numA > 0) {
            totalAlumnos += numA;
            rl.question(
              `Introduce las notas de los ${numA} alumnos del curso ${i} separadas por espacios: `,
              (notas) => {
                const notasArr = notas.split(" ").map(Number);
                if (notasArr.length === numA && !notasArr.includes(NaN)) {
                  cursos.push(notasArr);
                  totalNotas += notasArr.reduce((a, b) => a + b, 0);
                  if (cursos.length === num) {
                    const media = totalNotas / totalAlumnos;
                    console.log(
                      `La media de notas de todos los alumnos es: ${media.toFixed(
                        2
                      )}`
                    );
                    rl.close();
                  }
                } else {
                  console.log(
                    `Por favor, introduce ${numA} notas válidas para el curso ${i}.`
                  );
                  rl.close();
                }
              }
            );
          } else {
            console.log(
              `Por favor, introduce un número válido de alumnos para el curso ${i}.`
            );
            rl.close();
          }
        }
      );
    }
  } else {
    console.log("Por favor, introduce un número válido de cursos.");
    rl.close();
  }
});
//Emiliano Loya Flores
