// 20.- Calcular el factorial de un número, mediante funciones.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,

  output: process.stdout,
});

const factorial = (num) => {
  if (num === 0 || num === 1) {
    return 1;
  }
  return num * factorial(num - 1);
};

rl.question("Introduce un número para calcular su factorial: ", (numero) => {
  const num = parseInt(numero);
  if (!isNaN(num) && num >= 0) {
    console.log(`El factorial de ${num} es: ${factorial(num)}`);
  } else {
    console.log("Por favor, introduce un número válido.");
  }
  rl.close();
});
//Emiliano Loya Flores
