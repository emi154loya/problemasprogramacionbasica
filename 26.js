// 26.- Introducir una frase por teclado e imprimirla en el centro de la pantalla.

const readline = require("readline");
const { stdout } = require("process");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Introduce una frase: ", (frase) => {
  const terminalWidth = stdout.columns;
  const fraseLength = frase.length;
  const padding = Math.floor((terminalWidth - fraseLength) / 2);
  console.log(" ".repeat(padding) + frase);
  rl.close();
});
//Emiliano Loya Flores
