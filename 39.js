// 39.- Crear una tabla de 3 paginas, 4 filas y 5 columnas donde el primer elemento valga 1, el
// segundo 2, el tercero 3 y así sucesivamente.

// Función para crear una tabla de páginas, filas y columnas dadas
const crearTabla = (paginas, filas, columnas) => {
  let valor = 1;
  const tabla = [];
  for (let p = 0; p < paginas; p++) {
    tabla[p] = [];
    for (let i = 0; i < filas; i++) {
      tabla[p][i] = [];
      for (let j = 0; j < columnas; j++) {
        tabla[p][i][j] = valor++;
      }
    }
  }
  return tabla;
};

// Ejemplo de uso
const paginas = 3;
const filas = 4;
const columnas = 5;

const tablaEjemplo = crearTabla(paginas, filas, columnas);
console.log("Tabla generada:");
console.table(tablaEjemplo);
//Emiliano Loya Flores
