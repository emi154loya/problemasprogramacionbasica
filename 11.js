// 11.- Imprimir y contar los múltiplos de 3 desde el uno hasta un número que introducimos por teclado.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Introduce un número: ", (limite) => {
  let contadorMultiplos = 0;
  for (let i = 1; i <= limite; i++) {
    if (i % 3 === 0) {
      console.log(i);
      contadorMultiplos++;
    }
  }
  console.log(
    "Hay " + contadorMultiplos + " múltiplos de 3 hasta " + limite + "."
  );
  rl.close();
});
//Emiliano Loya Flores
