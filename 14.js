// 14.- Hacer un programa que imprima el mayor y el menor de una serie de cinco números que vamos introduciendo por teclado.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let mayor = Number.MIN_SAFE_INTEGER;
let menor = Number.MAX_SAFE_INTEGER;
let contador = 0;

const leerNumero = () => {
  rl.question("Introduce un número: ", (numero) => {
    contador++;
    if (numero > mayor) {
      mayor = numero;
    }
    if (numero < menor) {
      menor = numero;
    }
    if (contador < 5) {
      leerNumero();
    } else {
      console.log("El mayor número es: " + mayor);
      console.log("El menor número es: " + menor);
      rl.close();
    }
  });
};

leerNumero();
//Emiliano Loya Flores
