// 8.- Hacer un programa que solo nos permita introducir S o N.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const leerRespuesta = () => {
  rl.question("Introduce S o N: ", (respuesta) => {
    if (respuesta.toUpperCase() === "S" || respuesta.toUpperCase() === "N") {
      console.log("Respuesta válida: " + respuesta.toUpperCase());
      rl.close();
    } else {
      console.log("Por favor, introduce solo S o N.");
      leerRespuesta();
    }
  });
};

leerRespuesta();
//Emiliano Loya Flores
