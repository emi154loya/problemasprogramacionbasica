// 15.- Introducir dos números por teclado. Imprimir los números naturales que hay entre ambos
// números empezando por el más pequeño, contar cuántos hay y cuántos de ellos son pares.
// Calcular la suma de los impares.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let contador = 0;
let contadorPares = 0;
let sumaImpares = 0;

rl.question("Introduce el primer número: ", (num1) => {
  rl.question("Introduce el segundo número: ", (num2) => {
    num1 = parseInt(num1);
    num2 = parseInt(num2);
    for (let i = Math.min(num1, num2); i <= Math.max(num1, num2); i++) {
      console.log(i);
      contador++;
      if (i % 2 === 0) {
        contadorPares++;
      } else {
        sumaImpares += i;
      }
    }
    console.log("Hay " + contador + " números entre " + num1 + " y " + num2);
    console.log("Hay " + contadorPares + " números pares.");
    console.log("La suma de los números impares es: " + sumaImpares);
    rl.close();
  });
});
//Emiliano Loya Flores
