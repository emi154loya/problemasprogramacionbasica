// 41. Hacer un programa que nos permita dar altas en el archivo DATOS.DAT, cuyos campos son:
// ID, NOMBRE, APELLIDOS, DIRECCIÓN y ESTADO.

// Suponiendo que estamos trabajando en un entorno de Node.js y tenemos acceso a módulos para manipular archivos

const fs = require("fs");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const agregarRegistro = () => {
  rl.question("Introduce el ID: ", (id) => {
    rl.question("Introduce el nombre: ", (nombre) => {
      rl.question("Introduce los apellidos: ", (apellidos) => {
        rl.question("Introduce la dirección: ", (direccion) => {
          rl.question("Introduce el estado: ", (estado) => {
            const registro = `${id},${nombre},${apellidos},${direccion},${estado}\n`;
            fs.appendFile("DATOS.DAT", registro, (err) => {
              if (err) throw err;
              console.log("Registro agregado correctamente.");
              rl.close();
            });
          });
        });
      });
    });
  });
};

agregarRegistro();
//Emiliano Loya Flores
