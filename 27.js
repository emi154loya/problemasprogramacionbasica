// 27.- Realizar la tabla de multiplicar de un numero entre 0 y 10.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Introduce un número entre 0 y 10: ", (numero) => {
  const num = parseInt(numero);
  if (!isNaN(num) && num >= 0 && num <= 10) {
    console.log(`Tabla de multiplicar del ${num}:`);
    for (let i = 1; i <= 10; i++) {
      console.log(`${num} * ${i} = ${num * i}`);
    }
  } else {
    console.log("Por favor, introduce un número válido entre 0 y 10.");
  }
  rl.close();
});
//Emiliano Loya Flores
