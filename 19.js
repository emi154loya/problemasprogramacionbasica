// 19.- Hacer un programa que simule el funcionamiento de un reloj digital y que permita ponerlo en
// hora.

// Función para obtener la hora actual
function obtenerHoraActual() {
  const fecha = new Date();
  const horas = formatoDosDigitos(fecha.getHours());
  const minutos = formatoDosDigitos(fecha.getMinutes());
  const segundos = formatoDosDigitos(fecha.getSeconds());

  return `${horas}:${minutos}:${segundos}`;
}

// Función para dar formato de dos dígitos a los números
function formatoDosDigitos(numero) {
  return numero < 10 ? "0" + numero : numero;
}

// Función para imprimir el reloj
function imprimirReloj() {
  console.clear();
  console.log("Reloj Digital");
  console.log(obtenerHoraActual());
}

// Inicialización de variables
let hora, minuto, segundo;

// Función principal
function iniciarReloj() {
  // Obtener la hora actual
  const fechaActual = new Date();
  hora = fechaActual.getHours();
  minuto = fechaActual.getMinutes();
  segundo = fechaActual.getSeconds();

  // Imprimir el reloj
  setInterval(() => {
    imprimirReloj();
    segundo++;
    if (segundo === 60) {
      segundo = 0;
      minuto++;
      if (minuto === 60) {
        minuto = 0;
        hora++;
        if (hora === 24) {
          hora = 0;
        }
      }
    }
  }, 1000);
}

// Iniciar el reloj
iniciarReloj();
//Emiliano Loya Flores
