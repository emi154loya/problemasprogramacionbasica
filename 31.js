// 31.- Hacer un programa que nos permita introducir un número por teclado y sobre él se realicen
// las siguientes operaciones: comprobar si es primo, hallar su factorial o imprimir su tabla de
// multiplicar.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const esPrimo = (num) => {
  if (num <= 1) return false;
  if (num <= 3) return true;
  if (num % 2 === 0 || num % 3 === 0) return false;
  let i = 5;
  while (i * i <= num) {
    if (num % i === 0 || num % (i + 2) === 0) return false;
    i += 6;
  }
  return true;
};

const factorial = (num) => {
  if (num === 0 || num === 1) {
    return 1;
  }
  return num * factorial(num - 1);
};

rl.question("Introduce un número: ", (numero) => {
  const num = parseInt(numero);
  if (!isNaN(num) && num >= 0) {
    if (esPrimo(num)) {
      console.log(`${num} es primo.`);
    } else {
      console.log(`${num} no es primo.`);
    }
    console.log(`El factorial de ${num} es: ${factorial(num)}`);
    console.log(`Tabla de multiplicar de ${num}:`);
    for (let i = 1; i <= 10; i++) {
      console.log(`${num} * ${i} = ${num * i}`);
    }
  } else {
    console.log("Por favor, introduce un número válido mayor o igual a 0.");
  }
  rl.close();
});
//Emiliano Loya Flores
