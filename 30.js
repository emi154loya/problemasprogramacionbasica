// 30.- Introducir dos números por teclado y mediante un menú, calcule su suma, su resta, su
// multiplicación o su división.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Introduce el primer número: ", (num1) => {
  rl.question("Introduce el segundo número: ", (num2) => {
    const n1 = parseFloat(num1);
    const n2 = parseFloat(num2);
    if (!isNaN(n1) && !isNaN(n2)) {
      rl.question("Selecciona una operación (+, -, *, /): ", (operacion) => {
        let resultado;
        switch (operacion) {
          case "+":
            resultado = n1 + n2;
            break;
          case "-":
            resultado = n1 - n2;
            break;
          case "*":
            resultado = n1 * n2;
            break;
          case "/":
            if (n2 !== 0) {
              resultado = n1 / n2;
            } else {
              console.log("No se puede dividir por cero.");

              rl.close();
              return;
            }
            break;
          default:
            console.log("Operación no válida.");
            rl.close();
            return;
        }
        console.log(
          `El resultado de ${n1} ${operacion} ${n2} es: ${resultado}`
        );
        rl.close();
      });
    } else {
      console.log("Por favor, introduce números válidos.");
      rl.close();
    }
  });
});
//Emiliano Loya Flores
