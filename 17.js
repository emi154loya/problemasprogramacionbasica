// 17.- Imprimir, contar y sumar los múltiplos de 2 que hay entre una serie de números, tal que el
// segundo sea mayor o igual que el primero.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Introduce el primer número: ", (num1) => {
  rl.question(
    "Introduce el segundo número (mayor o igual al primero): ",
    (num2) => {
      num1 = parseInt(num1);
      num2 = parseInt(num2);
      let contadorMultiplos = 0;
      let sumaMultiplos = 0;
      for (let i = num1; i <= num2; i++) {
        if (i % 2 === 0) {
          console.log(i);
          contadorMultiplos++;
          sumaMultiplos += i;
        }
      }
      console.log("Se encontraron " + contadorMultiplos + " múltiplos de 2.");
      console.log("La suma de los múltiplos de 2 es: " + sumaMultiplos);
      rl.close();
    }
  );
});
//Emiliano Loya Flores
