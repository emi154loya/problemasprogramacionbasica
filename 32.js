// 32.- Crear un arreglo de 20 elementos con nombres de personas. Mostrar los elementos de la lista
// debiendo ir cada uno en una fila distinta.

const nombres = [
  "Ana",
  "Juan",
  "María",

  "Carlos",
  "Laura",
  "Pedro",
  "Sofía",
  "Diego",
  "Elena",
  "Miguel",
  "Lucía",
  "Pablo",
  "Carmen",
  "Javier",
  "Valeria",
  "David",
  "Raquel",
  "Alejandro",
  "Isabel",
  "Daniel",
];

console.log("Lista de nombres:");
for (let nombre of nombres) {
  console.log(nombre);
}
//Emiliano Loya Flores
