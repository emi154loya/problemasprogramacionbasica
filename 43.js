// 43.- Dado el archivo DATOS.DAT, realizar un programa que nos permita realizar modificaciones
// cuantas veces deseemos.

// Suponiendo que estamos trabajando en un entorno de Node.js y tenemos acceso a módulos para manipular archivos

const fs = require("fs");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const modificarRegistro = (id) => {
  fs.readFile("DATOS.DAT", "utf8", (err, data) => {
    if (err) throw err;
    const registros = data.split("\n");
    const registro = registros.find((reg) => {
      const campos = reg.split(",");
      return campos[0] === id;
    });
    if (registro) {
      console.log("Registro encontrado:", registro);
      rl.question(
        "Introduce los nuevos datos separados por comas (NOMBRE, APELLIDOS, DIRECCIÓN, ESTADO): ",
        (nuevosDatos) => {
          const nuevoRegistro = registro.split(",");
          const datosNuevos = nuevosDatos.split(",");
          nuevoRegistro[1] = datosNuevos[0];
          nuevoRegistro[2] = datosNuevos[1];
          nuevoRegistro[3] = datosNuevos[2];
          nuevoRegistro[4] = datosNuevos[3];
          const nuevoRegistroStr = nuevoRegistro.join(",");
          const nuevosRegistros = registros
            .map((reg) => {
              if (reg === registro) return nuevoRegistroStr;
              return reg;
            })
            .join("\n");
          fs.writeFile("DATOS.DAT", nuevosRegistros, "utf8", (err) => {
            if (err) throw err;
            console.log("Registro modificado correctamente.");
            rl.close();
          });
        }
      );
    } else {
      console.log("Registro no encontrado.");
      rl.close();
    }
  });
};

rl.question("Introduce el ID del registro a modificar: ", (id) => {
  modificarRegistro(id);
});
//Emiliano Loya Flores
