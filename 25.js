// 25.- Introducir un número menor de 5000 y pasarlo a numero romano.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const convertirARomano = (num) => {
  const numeros = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  const romanos = [
    "M",
    "CM",
    "D",
    "CD",
    "C",
    "XC",
    "L",
    "XL",
    "X",
    "IX",
    "V",
    "IV",
    "I",
  ];
  let resultado = "";
  for (let i = 0; i < numeros.length; i++) {
    while (num >= numeros[i]) {
      resultado += romanos[i];
      num -= numeros[i];
    }
  }
  return resultado;
};

rl.question("Introduce un número menor de 5000: ", (numero) => {
  const num = parseInt(numero);
  if (!isNaN(num) && num > 0 && num < 5000) {
    console.log(
      `El número romano correspondiente a ${num} es: ${convertirARomano(num)}`
    );
  } else {
    console.log("Por favor, introduce un número válido menor de 5000.");
  }
  rl.close();
});
//Emiliano Loya Flores
