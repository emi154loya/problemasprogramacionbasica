// 42.- Hacer un programa que nos permita dar bajas en el fichero DATOS.DAT.

// Suponiendo que estamos trabajando en un entorno de Node.js y tenemos acceso a módulos para manipular archivos

const fs = require("fs");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const eliminarRegistro = (id) => {
  fs.readFile("DATOS.DAT", "utf8", (err, data) => {
    if (err) throw err;
    const registros = data.split("\n");
    const nuevoRegistro = registros
      .filter((registro) => {
        const campos = registro.split(",");
        return campos[0] !== id;
      })
      .join("\n");
    fs.writeFile("DATOS.DAT", nuevoRegistro, "utf8", (err) => {
      if (err) throw err;
      console.log("Registro eliminado correctamente.");
      rl.close();
    });
  });
};

rl.question("Introduce el ID del registro a eliminar: ", (id) => {
  eliminarRegistro(id);
});
//Emiliano Loya Flores
