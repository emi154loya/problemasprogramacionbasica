// 36.- Generar una matriz de 4 filas y 5 columnas con números aleatorios entre 1 y 100, y hacer su
// matriz transpuesta.

const matriz = [];

for (let i = 0; i < 4; i++) {
  matriz[i] = [];
  for (let j = 0; j < 5; j++) {
    matriz[i][j] = Math.floor(Math.random() * 100) + 1;
  }
}

console.log("Matriz original:");
for (let fila of matriz) {
  console.log(fila.join("\t"));
}

const transpuesta = matriz[0].map((col, i) => matriz.map((row) => row[i]));

console.log("\nMatriz transpuesta:");
for (let fila of transpuesta) {
  console.log(fila.join("\t"));
}
//Emiliano Loya Flores
