// 29.- Simular cien tiradas de dos dados y contar las veces que entre los dos suman 10.

const contarSuma10 = () => {
  let contador = 0;
  for (let i = 0; i < 100; i++) {
    const dado1 = Math.floor(Math.random() * 6) + 1;
    const dado2 = Math.floor(Math.random() * 6) + 1;

    if (dado1 + dado2 === 10) {
      contador++;
    }
  }
  return contador;
};

console.log(
  "Cantidad de veces que la suma de dos dados es 10: " + contarSuma10()
);
//Emiliano Loya Flores
