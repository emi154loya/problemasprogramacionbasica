//7.- Introducir tantas frases como queramos y contarlas.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let contadorFrases = 0;

const leerFrase = () => {
  rl.question(
    "Introduce una frase (presiona Enter para finalizar): ",
    (frase) => {
      if (frase !== "") {
        contadorFrases++;
        leerFrase();
      } else {
        console.log("Se introdujeron " + contadorFrases + " frases.");
        rl.close();
      }
    }
  );
};

leerFrase();
//Emiliano Loya Flores
