// 28.- Simular el lanzamiento de una moneda al aire e imprimir cara o cruz.

const tirarMoneda = () => {
  return Math.random() < 0.5 ? "Cara" : "Cruz";
};

console.log("Resultado del lanzamiento de la moneda: " + tirarMoneda());
//Emiliano Loya Flores
