// 33.- Hacer un programa que lea las calificaciones de un alumno en 10 asignaturas, las almacene
// en y calcule e imprima su media.

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const calcularMedia = (calificaciones) => {
  let suma = 0;
  for (let calificacion of calificaciones) {
    suma += calificacion;
  }
  return suma / calificaciones.length;
};

let calificaciones = [];

rl.question(
  "Introduce las calificaciones del alumno en 10 asignaturas separadas por espacios: ",
  (input) => {
    calificaciones = input.split(" ").map(Number);
    if (calificaciones.length === 10 && !calificaciones.includes(NaN)) {
      console.log(
        `La media de las calificaciones es: ${calcularMedia(
          calificaciones
        ).toFixed(2)}`
      );
    } else {
      console.log(
        "Por favor, introduce 10 calificaciones válidas separadas por espacios."
      );
    }
    rl.close();
  }
);
//Emiliano Loya Flores
