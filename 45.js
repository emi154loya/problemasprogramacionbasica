// 45.- Tenemos el archivo DATOS.DAT con la misma estructura anterior, que esta indexado por el
// campo ID. Crear un programa que nos permita consultar un registro siempre que queramos.

// Suponiendo que estamos trabajando en un entorno de Node.js y tenemos acceso a módulos para manipular archivos

const fs = require("fs");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const consultarRegistroPorID = (id) => {
  fs.readFile("DATOS.DAT", "utf8", (err, data) => {
    if (err) throw err;
    const registros = data.split("\n");
    const registro = registros.find((reg) => {
      const campos = reg.split(",");
      return campos[0] === id;
    });
    if (registro) {
      console.log("Registro encontrado:", registro);
    } else {
      console.log("Registro no encontrado.");
    }
    rl.close();
  });
};

rl.question("Introduce el ID del registro a consultar: ", (id) => {
  consultarRegistroPorID(id);
});
//Emiliano Loya Flores
